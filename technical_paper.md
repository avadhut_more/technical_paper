# NoSQL DataBase
* ## Introduction :-
  + #### What is NoSQL Database ? 
    +  Also reffered as **non-sql Database** or **non-relationalDtabase**.
    +  NoSQL Database is a non-relational Data Management System.
    ![N|Solid](https://user.oc-static.com/upload/2019/07/05/15623398368289_SQL%20vs%20NoSQL.png#)
    + NoSQL database can store **structured**, **semi-structured**, **unstructured** and **polymorphic** data.
    ![N|Solid](https://www.m-files.com/blog/wp-content/uploads/2019/09/structured-vs-unstructured-data.png)
<br>

* ## Types of NoSQL Database
  1. #### Key-Value Stores :- 
       - Stores data as a hash table all keys as unique and values assigned to it can be string, JSON, BLOB, etc.
       - Ideal to store session information, user profile, etc.
       - Data insertion and retrieval are simple.
       - Example - 
          1. Amazon DynamoDB
          2. Oracle NoSQL Database
          3. Aerospike
           
       ![](http://www.ingenioussql.com/wp-content/uploads/2013/02/KeyValueStore.gif) 

   1. #### Document Stores :-
        -  Designed to store and query data as **JSON** like document.
        -  It is a collection of documents and each document is collection of **key-value** pairs.
        -  It is flexible, semistructured database.
        -  Example -
           1. MangoDB
           2. Couchbase
           3. RevenDB
            
        ![](https://www.rackone.it/wp-content/uploads/2015/02/tipo-nosql-document-databases-store.jpg) 

    1. #### Column-Oriented :-
        - Column-oriented databases stores data in column specific files with the same type.
        - All data associated with the single column is ideal for compression because of the same type.
        - Column-oriented databases basically designed to work on columns, every column is treated separately.
        -  Example - 
           1. Apache Cassandra
           2. Hadoop HBase
           3. HyperTable

        ![](https://phoenixnap.com/kb/wp-content/uploads/2020/05/column-oriented-database.png)

    1. #### Graph Network :-
        - Graph network database stores data in a graph like structure.
        - Graph databases used for storing frequently changing data.
        - Graph databases offer fast query performance than relational databases, especially as data grows.
        - A graph database is a collection of one or more nodes and edges.
        -  Example - 
           1. FlockDB
           2. Neo4j
           3. HyperGraphDB

        ![](https://upload.wikimedia.org/wikipedia/commons/3/3a/GraphDatabase_PropertyGraph.png)
<br>

* ## MongoDB
    - Database Type - Document.
    - Developed By- MangoDB Inc.
    - Instead of using tables and rows as in the relational databases, MongoDB uses collections and documents.
    - Key-Value pair is a basic unit of data in MongoDB.
    - The hierarchical relationships can be easily implemented in MongoDB.
    - The fields of MangoDB are
    ![](https://image.slidesharecdn.com/mongodb3modelinglinkedin-160123055702/95/mongodb-3-type-modeling-in-production-5-638.jpg?cb=1453528678) 
    - #### Main components of MongoDB are :-
      1. The **_id** field is added by MongoDB to uniquely identify the document in the collection.
      2. **Field** is a name-value pair in the document.A document can have many fields in it.
      3. **Document** is a set of value-pair fields.
      4. **Collection** is a set of documents in the database.
   
   - #### Why to use MongoDB :-
     - ##### Sharding :-
       * In this method data is distributed over multiple machines.
       * Horizontal scalability is provided by this.
       ![](https://i2.wp.com/www.kenwalger.com/blog/wp-content/uploads/2017/06/sharding-example.png?w=600&ssl=1)
       
       * Three parts of shared cluster are shards, query router, and config server.
       * The config servers store all of the metadata about the cluster
       * Query router, acts as the interface between the application and the data.
       * Using mangos query is routed to all shards unless to determines data is in which shard. 
       ![](https://i0.wp.com/www.kenwalger.com/blog/wp-content/uploads/2017/06/shard-broadcast-operation.png)
       
       * Migrating to a larger server may require application downtime which is not in sharding type.

     - ##### Query Faster :-
       - In the relational database as the size of the table goes on increasing queries become complex and the joining of the table becomes more expensive.
       - In MongoDB data that need to be accessed together are stored together so the query goes faster.
       ![](https://developer.mongodb.com/images/article/sql-to-mdb/dbnojoins.png) 

     - ##### Pivot Easier :-
       - This refers to requirement change.
       - Sometimes the changes can be challenging, time-consuming, and complex.
       - Adding a new table in the relational database needs to alter the table may take more time depending on the amount of data.
       - In MongoDB as everything is stored in the document and fields so it is easier to make changes through it.

* ## Cassandra
  * Database Type - Column-oriented
  * Developed by - Apache Software Foundation
  ![](https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2F1.bp.blogspot.com%2F_j6mB7TMmJJY%2FTK1npAatLqI%2FAAAAAAAAAd4%2FTscPInSeUoo%2Fs1600%2Fp1.png&f=1&nofb=1) 
  * It supports Linear scalability and proven to be fault-tolerance.
  * It supports replicating across multiple datacenters
  * #### Why to use Cassandra :-
    * ##### Highly Fault Tolerant :-
      * Cassandra has many mechanisms for fault tolerance.
      * Cassandra allows you to replicate your data to other data centers and keep multiple copies in multiple locations.
      * Cassandra is masterless, there is no single point of failure.
      * Cassandra follows a peer-to-peer architecture

    * ##### Elastic Scalability :-
      * Cassandra cluster can be easily scaled-up or scaled-down.
      * Any number of nodes can be added or deleted in the Cassandra cluster without much disturbance.
      * Zero downtime or any pause to the applications after the update.

    * #### Column-Oriented :-
      * Cassandra stores columns based on the column
      * Due to this quick slicing can be done.

* ## Apache CouchDB
  * Database Type - Document-oriented.
  * Developed by - Apache Software Foundation.
  ![](https://www.researchgate.net/profile/Rabi_Padhy/publication/265062016/figure/fig1/AS:295840460623874@1447545270861/Simple-Architecture-of-Apache-CouchDB-database-Storage-Engine-It-is-B-tree-based-and-the.png) 
  * It uses JSON to store data.
  * Key-Value pair is basic unit of data in it.
  * #### Why to use Apache CouchDB :-
    * ##### Scalability :-
      * The data can be scaled on different nodes.
      * It support horizontal partitioning and replication.

    * ##### No read locks :-
      * In most of the relational database if we want to modify or update the row of data row being changed will be locked.
      * This makes accessibility the issue for the user.
      * CouchDB uses MVCC (Multi-Version Concurrency Control).
      * Because of this CouchDB can run at full speed regardless of the current load.

* ## Apache HBase
  * Database Type - Column-oriented.
  ![](https://dwgeek.com/wp-content/uploads/2017/09/Apache-HBase-Data-Model-Explanation.jpg) 
  * An HBase system is designed to scale linearly.
  * In HBase each table must have an element defined as a primary key.
  * All attempts done to HBase tables must use the primary key.
  * HBase is very effective for handling large, sparse datasets.
  * #### Why Apache HBase :-
    * The data is stored in individual columns, and indexed by a unique row key. This allows for rapid retrieval of individual rows and columns.
    * ##### Scalable :-
      * HBase is designed to handle scaling across thousands of servers.
      * It can handle petabytes of data.

    * ##### Fast :-
      * HBase distributes from application to cluster of hosts so results to faster operating.
    
    * ##### Consistency :-
      * For high-speed requirements, we can use it since it offers consistent reads and writes. 

* ## neo4j :-
  * Database Type - Graph.
  * Developed by - neo4j.
  * This is used to manage interconnected data.
  * The database uses pointers to navigate and traverse the graph.
  ![](https://apiko.com/blog/content/images/2019/04/connections.png) 
  * #### Why to use neo4j :-
    * It is very easy and faster to retrieve/traversal/navigation of more Connected data.
    * ##### Manage relationships between data :-
      * With Neo4j you can explore different paths and connections between your data.
      * You can also easily fetch complex data from the database even if they are highly connected.

    * #### High Performance :- 
      * Neo4j scales horizontally.
      * Performance is not dependant on database size due to cluster.
   
    * #### ACID properties :-
      * It uses ACID properties.
      ![](https://media.geeksforgeeks.org/wp-content/cdn-uploads/20191121102921/ACID-Properties.jpg) 

* ## Conclusion :-
  * NoSQL databases use dynamic schema for unstructured data.
  * NoSQL databases are horizontally scalable.
  * NoSQL is a very good fit for hierarchical data storage as it stores the data in the form of key-value pairs in JSON format.  
